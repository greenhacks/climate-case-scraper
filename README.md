# Climate Case Scraper

## Overview
This is a proof-of-concept project. It will be a simple webscraper that provides a high-level overview of an existing website describing climate litigation, regulatory proceedings, and more surrounding US utilities.

## Proof of Concept
- Model-View-Controller (MVC)-style architecture
- Users will be able to navigate a website / graphical user interface (GUI)
- Users can click a button and scrape the source website
- Users will be able to download the scraped data as a CSV

## Tech / Library Stack
- Back end: Python, Beautiful Soup, Flask
- Front end: React (set up with Vite) / Typescript, Bootstrap
- Others as needed

## Data Source
http://climatecasechart.com/case-category/utility-regulation/



