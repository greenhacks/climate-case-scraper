import { useState } from 'react'
import Button from './components/Button';
import './App.css'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <div>
        Welcome to Climate Case Scraper!
      </div>
      <div>
        <Button 
        border = "solid"
        color = "#fdffc4"
        height = "70px"
        onClick={() => console.log("Test")}
        radius = "50%"
        width = "80px"
        children = "Scrape"
        />
      </div>
    </div>
  )
}

export default App
