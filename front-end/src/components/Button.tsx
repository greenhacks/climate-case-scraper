import React from "react";

interface ButtonProps { // TypeScript type annotation describing the props
  border: string;
  color: string;
  children?: React.ReactNode;
  height: string;
  onClick: () => void;
  radius: string
  width: string;
  }


function Button({border, color, children, height, onClick, width}: ButtonProps) {
  return(
  <button 
      onClick={onClick}
      style={{
        backgroundColor: color,
        border,
        height,
        width
      }}
    >
    {children}
    </button>
    )
}

export default Button;