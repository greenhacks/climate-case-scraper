import requests
from bs4 import BeautifulSoup

# Step 1) create request variable
req = requests.get('http://climatecasechart.com/case-category/utility-regulation/')

# Step 2) Create Beautiful Soup Object
soup = BeautifulSoup(req.text,'html.parser')

# # Step 3) Find classes and tags
case_title_list = soup.find_all(class_="entry-title")
case_title_list_items = soup.find_all("a")

# Step 4) Write a for loop and pull contents from tag
for case_title in case_title_list_items:
    titles = case_title.contents[0]
    print(titles)